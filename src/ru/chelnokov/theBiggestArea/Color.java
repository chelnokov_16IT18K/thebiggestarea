package ru.chelnokov.theBiggestArea;

public enum Color {
    RED,
    ORANGE,
    YELLOW,
    GREEN,
    BLUE,
    PURPLE,
    VIOLET,
    WHITE,
    BLACK,
    GRADIENT
}
