package ru.chelnokov.theBiggestArea;

public class Point {

    private double x;
    private double y;

    Point(double x, double y){
        this.x = x;
        this.y = y;
    }
    Point(){
        this(0,0);
    }
    double getX(){
        return x;
    }

    double getY() {
        return y;
    }

    public String toString(){
        return "(" + x + ", " + y + ")";
    }
}
