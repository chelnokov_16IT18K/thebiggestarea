package ru.chelnokov.theBiggestArea;

public abstract class  Shape{
    private Color color;
    Shape(Color color){
        this.color = color;
    }
    Color getColor(){
        return color;
    }
    public abstract double area();
}

