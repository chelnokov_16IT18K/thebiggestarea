package ru.chelnokov.theBiggestArea;

public class Main {
    public static void  main(String[] args){
        Circle circle = new Circle(Color.BLACK, new Point(), 5);
        Triangle triangle= new Triangle(Color.BLUE, new Point(0,0), new Point(-1,1), new Point(1,2));
        Square square = new Square(Color.RED, new Point(1,1), 5);

        Shape shape = triangle;
        Object o = triangle;
        Triangle triangle1 = (Triangle) o;

        Shape[] shapes = {circle, triangle, square};
        printArrayElements(shapes);

        Shape maxShape = maxShapeArea(shapes);
        System.out.println("Фигура с наибольшей площадью: " + maxShape);
    }
    private static void printArrayElements(Object[] objects){
        for (Object objeckt : objects){
            System.out.println(objeckt);
        }
    }


    private static Shape maxShapeArea(Shape[] shapes) {
        Shape maxShape = null;
        double maxArea = Double.NEGATIVE_INFINITY;
        for(Shape shape : shapes){
            double area = shape.area();
            if(area > maxArea){
                maxShape = shape;
            }
        }
        return maxShape;
    }

}
