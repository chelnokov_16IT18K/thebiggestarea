package ru.chelnokov.theBiggestArea;

public class Square extends  Shape {
    private Point corner;
    private double side;
    Square(Color color, Point corner, double side){
        super(color);
        this.corner = corner;
        this.side = side;
    }

    @Override
    public double area() {
        return side * side;
    }

    @Override
    public String toString() {
        return "Квадрат {" + " Цвет " + getColor() + " Угловая точка " + corner + " Сторона " + side + '}';
    }
}
