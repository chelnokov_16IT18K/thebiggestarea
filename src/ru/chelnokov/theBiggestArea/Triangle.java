package ru.chelnokov.theBiggestArea;

public class Triangle extends Shape {
    private Point a;
    private Point b;
    private Point c;
    Triangle(Color color, Point a, Point b, Point c){
        super(color);
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public double area() {
        return Math.abs( 0.5 * ((a.getX() - c.getX())*(b.getY() - c.getX()) - (b.getX() - c.getX()) * (a.getY() - c.getY())));
    }

    @Override
    public String toString() {
        return "Треугольник {" + " Вершина 1 " + a + " Вершина 2 " + b + " Вершина 3 " + c + '}';
    }
}
